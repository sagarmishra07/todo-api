const router = require("express").Router();

const Todo = require("../models/Todo");

router.get("/todos", (req, res) => {
  // Todo.find((err, todos) => {
  //   // if (err) {
  //   //   res.send(err);
  //   // }else{
  //   //   res.json(todos);
  //   // }
  // });

  Todo.find()
    .populate("user")
    .then((todos) => res.json(todos))
    .catch((error) => res.json(error.message));
});
/**
 * Get a todo by its ID.
 * @param {Request} req - The request object.
 * @param {Response} res - The response object.
 * @returns None
 */

router.get("/todos/:todoID", (req, res) => {
  Todo.findById({ _id: req.params.todoID }, (err, val) => {
    if (err) {
      res.send(err.message);
    }
    res.json(val);
  });
});

/**
 * A route that searches for a todo based on the title.
 * @param {Request} req - The request object.
 * @param {Response} res - The response object.
 * @returns None
 */
router.get("/todos/search/:title", (req, res) => {
  Todo.find((err, todos) => {
    const data = todos.filter((val) => {
      if (val.title.toLowerCase().match(req.params.title.toLowerCase())) {
        return val;
      }
    });
    if (data) {
      res.json(data);
    } else {
      res.send(err);
    }
  });
});

/**
 * Takes in a new todo and adds it to the database.
 * @param {Request} req - the request object
 * @param {Response} res - the response object
 * @returns None
 */
router.post("/addtodos", (req, res) => {
  const data = new Todo({
    title: req.body.title,
    description: req.body.description,
    user: req.body.user,
  });
  data.save((err, val) => {
    if (err) {
      res.send(err.message);
    }
    res.json(val);
  });
});

/**
 * Update a todo item.
 * @param {string} todoID - the id of the todo item to update
 * @param {string} title - the new title of the todo item
 * @param {string} description - the new description of the todo item
 * @returns None
 */
router.put("/updatetodo/:todoID", (req, res) => {
  Todo.findById({ _id: req.params.todoID }, (err, val) => {
    val.title = req.body.title;
    val.description = req.body.description;
    try {
      val.save();
      res.json(val);
    } catch (error) {
      res.send(err.message);
    }
  });
});

/**
 * Update the status of a todo item.
 * @param {string} todoID - the id of the todo item to update
 * @param {string} status - the new status of the todo item
 * @returns None
 */
router.put("/updatestatus/:todoID", (req, res) => {
  Todo.findById({ _id: req.params.todoID }, (err, val) => {
    val.title = val.title;
    val.description = val.description;
    val.status = req.body.status;
    try {
      val.save();
      res.json(val);
    } catch (error) {
      res.send(err.message);
    }
  });
});

/**
 * Deletes a todo from the database.
 * @param {Request} req - the request object
 * @param {Response} res - the response object
 * @returns None
 */
router.delete("/delete/:todoID", (req, res) => {
  Todo.deleteOne({ _id: req.params.todoID })
    .then(() => res.send({ message: "Todo Deleted" }))
    .catch((err) => res.status(404).send(err.message));
});

module.exports = router;
