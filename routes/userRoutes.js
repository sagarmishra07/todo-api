const router = require("express").Router();

/**
 * Get all users from the database.
 * @param {Request} req - The request object.
 * @param {Response} res - The response object.
 * @returns None
 */
const User = require("../models/User");
router.get("/users", (req, res) => {
  User.find((err, todos) => {
    if (err) {
      res.send(err);
    }
    res.json(todos);
  });
});
/**
 * Adds a new user to the database.
 * @param {Request} req - The request object.
 * @param {Response} res - The response object.
 * @returns None
 */
router.post("/addusers", (req, res) => {
  const data = new User({
    username: req.body.username,
    email: req.body.email,
    password: req.body.password,
  });
  data.save((err, val) => {
    if (err) {
      res.send(err.message);
    }
    res.json(val);
  });
});
module.exports = router;
