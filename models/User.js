const mongoose = require("mongoose");

/**
 * The schema for the User model.
 * @param {string} username - the username of the user.
 * @param {string} email - the email of the user.
 * @param {string} password - the password of the user.
 * @param {Date} createdAt - the date the user was created.
 * @returns None
 */
const UserSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },

  createdAt: {
    type: Date,
    required: true,
    default: Date.now(),
  },
});

module.exports = new mongoose.model("User", UserSchema);
