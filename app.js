const express = require("express");
const mongoose = require("mongoose");
const app = express();
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
const dbURI =
  "mongodb+srv://sagar:sagar@cluster0.ft0he.mongodb.net/hostel-management?retryWrites=true&w=majority";
mongoose
  .connect(dbURI, {
    useNewUrlParser: true,

    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("connected");
  })
  .catch((err) => console.log(err));

//listen for request
app.use(require("./routes/index"));
app.use(require("./routes/userRoutes"));
app.listen(3000);

// app.get("/about", (req, res) => {
//   res.sendFile("./views/about.html", { root: __dirname });
// });

// app.get("/about-us", (req, res) => {
//   res.redirect("about");
// });

// app.use((req, res) => {
//   res.sendFile("./views/404.html", { root: __dirname });
// });
